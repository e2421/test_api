const nodemailer = require('nodemailer');

module.exports = class Pay {
    constructor(type, plural) {
        this.type = type;
        this.plural = plural;
    }

    sendMail() {
        return {
            auth: false,
            handler: async(request, h) => {
                try {
                    // console.log('Notre Request Query :', request.payload);
                    let resp = {};

                    let transporter = nodemailer.createTransport({
                        host: 'mail.privateemail.com',
                        // service: 'gmail',
                        port: 465,
                        secure: true,
                        auth: {
                            user: 'support@worldbusiness.store',
                            pass: 'Support4*'
                        }
                    });

                    let mailOptions = {
                        from: request.payload.email,
                        // to: request.payload.email,//'bchryzal@gmail.com',
                        to: 'support@worldbusiness.store',
                        subject: request.payload.name + ' vous contact',
                        text: 'Je vous contacte...',
                        html: '<h2> <b>' + request.payload.name + '</b></h2> <br><br>' + request.payload.message + ' <br>',
                    };

                    transporter.sendMail(mailOptions, (err, info) => {
                        if (err) { return console.log('Error transporter sendmail :', err); }

                        console.log('Message sent infos :', info);
                        resp = info.messageId;
                        // console.log('Preview URL : ', nodemailer.getTestMessageUrl(info));

                    });

                    await new Promise(resolve => setTimeout(resolve, 2000)); // 3 sec

                    return h.response({
                        [this.plural]: resp,
                    }).code(200);
                } catch (error) {
                    return error;
                }
            },
            tags: ['api']
        };
    }

    welcomeMail() {
        return {
            auth: false,
            handler: async(request, h) => {
                try {
                    console.log('Notre Request Query :', request.payload);
                    let resp = {};

                    let transporter = nodemailer.createTransport({
                        host: 'mail.privateemail.com',
                        // service: 'gmail',
                        port: 465,
                        secure: true,
                        auth: {
                            user: 'support@worldbusiness.store',
                            pass: 'Support4*'
                        },
                        tls: { rejectUnauthorized: false }
                    });

                    let mailOptions = {
                        from: 'support@worldbusiness.store',
                        // to: 'oneany600@gmail.com', 
                        to: request.payload.email,
                        subject: 'Bienvenue dans World Business !',
                        // text: 'Hello Somebody',
                        html: `
                      <li>Titulaire: <b>` + request.payload.banque + `</b> </li>
                   `
                    };

                    transporter.sendMail(mailOptions, (err, info) => {
                        if (err) { return console.log('Error transporter sendmail :', err); }

                        console.log('Message sent infos :', info);
                        resp = info.messageId;
                        // console.log('Preview URL : ', nodemailer.getTestMessageUrl(info));
                    });

                    await new Promise(resolve => setTimeout(resolve, 4000)); // 3 sec

                    return h.response({
                        [this.plural]: resp,
                    }).code(200);
                } catch (error) {
                    return error;
                }
            },
            tags: ['api']
        };
    }

    verificationMail() {
        return {
            auth: false,
            handler: async(request, h) => {
                try {
                    console.log('Notre code :', request.payload.code);
                    console.log('Notre email :', request.payload.email);
                    let resp = {};

                    // let transporter = nodemailer.createTransport({
                    //     host: 'gmail',
                    //     // service: 'gmail',
                    //     //port: 465,
                    //     secure: true,
                    //     auth: {
                    //         user: 'xenooow@gmail@gmail.com',
                    //         pass: '(hdjhj45d!fuiIdf65d)'
                    //     },
                    //     tls: { rejectUnauthorized: false }
                    // });

                    // let mailOptions = {
                    //     from: 'xenooow@gmail@gmail.com',
                    //     to: request.payload.email,
                    //     subject: 'Activez votre nouveau compte !',
                    //     html: `
                    //       <html>
                    //       <body style="background-color: white; font-size: 10px;">
                    //           <div style="color: rgb(16, 48, 107); padding: 20px; background-color: #ecebeb">
                    //           <img width="20%" style="background-color: #ffffff; margin-left: 40%;"
                    //           src="https://worldbusiness.s3.af-south-1.amazonaws.com/world+business-02.png" />

                    //           <h2>Vous y êtes presque ! Il vous reste à valider votre adresse mail</h2> <br> 

                    //           Votre code de validation: {request.payload.code}  <br><br>

                    //           <span style="text-align: center; padding: 5px; background-color: #3bab38;
                    //               color: white; cursor: pointer;">
                    //               <a href="https://worldbusiness.store/verification/code"></a>
                    //               Rendez-vous sur notre site pour la validation de votre email avec votre code
                    //           </span>

                    //           <br><br>
                    //       </body>
                    //       <hr style="margin-top: 10px; margin-bottom: 10px;">
                    //       </html>
                    //     `
                    // };

                    // transporter.sendMail(mailOptions, (err, info) => {
                    //     if (err) { return console.log('Error transporter sendmail :', err); }

                    //     console.log('Message sent infos :', info);
                    //     resp = info.messageId;
                    //     // console.log('Preview URL : ', nodemailer.getTestMessageUrl(info));
                    // });

                    // await new Promise(resolve => setTimeout(resolve, 4000)); // 3 sec

                    return h.response({
                        //[this.plural]: resp,
                        message: "ok, in the controlleur "
                    }).code(200);
                } catch (error) {
                    return error;
                }
            },
            tags: ['api']
        };
    }

}
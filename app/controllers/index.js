const UserController = require('./user');
const ResourceController = require('./resource');
const validation = require('../validations');
const Mail = require('./mail');

module.exports = {
    admin: new UserController('admin', 'admins', validation.admin),

    mail: new Mail('mail', 'mails')
};
const Joi = require('joi');

module.exports = {
    register: {
        email: Joi.string().required(),
        password: Joi.string().required(),
        firstname: Joi.string().required(),
        lastname: Joi.string().required(),
        role: Joi.string().optional(),
        active: Joi.boolean().optional(),
    },
    registerFile: {
        fileprop: Joi.any().optional(),
        filenumber: Joi.any().optional(),
        objectdata: Joi.any().optional(),
        filedata0: Joi.any().optional(),

        // filelinks: Joi.any().optional(),
    },
    login: {
        email: Joi.string().required(),
        password: Joi.string().required(),
    },
    update: {
        email: Joi.string().optional(),
        password: Joi.string().optional(),
        firstname: Joi.string().optional(),
        lastname: Joi.string().optional(),

        role: Joi.string().optional(),
        active: Joi.boolean().optional(),
    },
    remove: {
        id: Joi.string().required()
    }
}